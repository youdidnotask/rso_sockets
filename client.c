#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <string.h>
#include "prtcl.h"

#define RESULT_ERROR -1

int sockfd;

void prepare_sqrt_request(struct prtcl* protocol_request, unsigned long id) {
	double value;

	printf("Enter a value: ");
	scanf("%lf", &value);
	
	protocol_request->type = TYPE_REQUEST;
	protocol_request->op = OP_SQRT;
	protocol_request->id = id;
	protocol_request->data.value =  doubleLittleToBigEndian(value);
}

void prepare_date_request(struct prtcl* protocol_request, unsigned long id) {
	protocol_request->type = TYPE_REQUEST;
	protocol_request->op = OP_DATE;
	protocol_request->id = id;
}

void signal_handler() {
	struct prtcl protocol_response;
	read(sockfd, &protocol_response, sizeof(struct prtcl));
	if(protocol_response.op == OP_SQRT) {
		puts("---------------------------------------");
		printf("Server response: %f\n", doubleBigToLittleEndian(protocol_response.data.value));
		puts("---------------------------------------");
	}
	else if(protocol_response.op == OP_DATE) {
		unsigned short length = uShortBigToLittleEndian(protocol_response.data.date.length);
		char* text = malloc(length * sizeof(char));
		strncpy(text, protocol_response.data.date.value, length);
		text[length] ='\0';
		puts("---------------------------------------");
		printf("Server response: %s\n", text);
		puts("---------------------------------------");
		free(text);
	}
}

int main(int argc, char* argv[]) {

	socklen_t len;										/* unsigned int 32bits */
	struct sockaddr_in address;
	int result;
	struct prtcl protocol_request;
	unsigned long id = 0;
	char choice;
	int process_id;

	printf("Client runs succesfully.\n");

	signal(SIGINT, signal_handler);

	/* Create a socket for the client */
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd == RESULT_ERROR) {
		printf("Create a socket error.\n");
	}

	/* Name the socket, as agreed with the server */
	address.sin_family = AF_INET;
	address.sin_port = htons(6666); 					/* htons() - host to network short */
	address.sin_addr.s_addr = inet_addr("127.0.0.1");	/* convert an IP address to a long format */
	len = sizeof(address);

	/* Connect the socket to the server's socket */
	result = connect(sockfd, (struct sockaddr*) &address, len);

	if(result == RESULT_ERROR) {
		printf("Connect to a server error.\n");
		exit(EXIT_FAILURE);
	}

	/* Communication */

	process_id = getpid();
	write(sockfd, &process_id, sizeof(process_id));
	while(choice != 3) {
		puts("SQRT REQUEST:\t[1]");
		puts("DATE REQUEST:\t[2]");
		puts("END:\t\t[3]");

		scanf(" %c", &choice);
		switch(choice) {
			case '1':	prepare_sqrt_request(&protocol_request, id++);
						write(sockfd, &protocol_request, sizeof(struct prtcl));
						break;
			case '2':	prepare_date_request(&protocol_request, id++);
						write(sockfd, &protocol_request, sizeof(struct prtcl));
						break;
			case '3':	close(sockfd);
						exit(EXIT_SUCCESS);
						break;
			default : 	puts("Unknown command");
		}
	}

	return 0;
}
