#ifndef PRTCL_H
#define PRTCL_H
#define TYPE_REQUEST 0
#define TYPE_RESPONSE 1
#define OP_SQRT 1
#define OP_DATE 2

struct date_info {
	unsigned short length;
	char value[24];
};

struct prtcl {
	unsigned type	: 1;
	unsigned bit1	: 1;
	unsigned bit2	: 1;
	unsigned op		: 4;
	unsigned long id;
	union _data {
		double value;
		struct date_info date;
	} data;
};

int isBigEndian() {
	uint16_t x = 0x0001;
	char c=*(char*)&x;
	return !c;
}

int isLittleEndian() {
	uint16_t x = 0x0001;
	char c=*(char*)&x;
	return c;
}

double doubleLittleToBigEndian(double value_le) {
	int i, j;
	double value_be;
	char* c_le = (char*)&value_le;
	char* c_be = (char*)&value_be;
	if(isLittleEndian()) {
		for(i=0, j=sizeof(double)-1; i<sizeof(double); i++, j--) {
			c_be[i] = c_le[j]; 
		}
		return value_be;
		
	}
	return value_le;
}

double doubleBigToLittleEndian(double value_be) {
	int i, j;
	double value_le;
	char* c_be = (char*)&value_be;
	char* c_le = (char*)&value_le;
	if(isLittleEndian()) {
		for(i=0, j=sizeof(double)-1; i<sizeof(double); i++, j--) {
			c_le[i] = c_be[j];
		}
		return value_le;
	}
	return value_be;
}

unsigned short uShortLittleToBigEndian(unsigned short value_le) {
	int i, j;
	unsigned short value_be;
	char* c_le = (char*)&value_le;
	char* c_be = (char*)&value_be;
	if(isLittleEndian()) {
		for(i=0, j=sizeof(unsigned short)-1; i<sizeof(unsigned short); i++, j--) {
			c_be[i] = c_le[j];
		}
		return value_be;
	}
	return value_le;
}

unsigned short uShortBigToLittleEndian(unsigned short value_be) {
	int i, j;
	unsigned short value_le;
	char* c_be = (char*)&value_be;
	char* c_le = (char*)&value_le;
	if(isLittleEndian()) {
		for(i=0, j=sizeof(unsigned short)-1; i<sizeof(unsigned short); i++, j--) {
			c_le[i] = c_be[j];
		}
		return value_le;
	}
	return value_be;
}

#endif
