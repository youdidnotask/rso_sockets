all: client server

client: client.o
	gcc -g -Wall -pedantic $^ -o $@ -lpthread

client.o: client.c prtcl.h
	gcc -g -c -Wall -pedantic $< -o $@ -lpthread

server: server.o
	gcc -g -Wall -pedantic $^ -o $@ -lpthread -lm

server.o: server.c prtcl.h
	gcc -g -c -Wall -pedantic $< -o $@ -lpthread -lm

.PHONY: clean

clean:
	-rm client.o server.o client server
