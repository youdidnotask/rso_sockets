/* STRUCTURES */
struct sockaddr_in {
	short			sin_family;		/* e.g. AF_INET, AF_INET6 */
	unsigned short	sin_port;		/* e.g. htons(3490) */
	struct in_addr	sin_addr;		
	char			sin_zero[8];	/* not needed */
};
struct in_addr {
	unsigned long s_addr;			/* IP address load with inet_pton() */
};
struct sockaddr {
	unsigned short	sa_family 		/* address family, AF_xxx */
	char			sa_data[14]		/* 14 bytes of protocol address */
};

/* FUNCTIONS */
int socket(int domain, int type, int protocol); 
	/* 
	address family:	AF_INET (IPv4)
	type:			SOCK_STREAM (connection oriented TCP protocol)
	protocol:		IPPROTO_IP == 0 (IP protocol)
	*/
int listen(int sockfd, int backlog);
	/*
	sockfd:			marks sockfd as a passive socket (socket used to accept incoming connections
	backlog:		maximum lengh of connections' queue
	*/

