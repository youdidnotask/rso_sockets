#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <signal.h>
#include "prtcl.h"

#define RESULT_ERROR -1


void prepare_sqrt_response(struct prtcl* protocol_request, struct prtcl* protocol_response) {
	double value;
	protocol_response->type = TYPE_RESPONSE;
	protocol_response->op = protocol_request->op;
	protocol_response->id = protocol_request->id;
	value = doubleBigToLittleEndian(protocol_request->data.value);
	protocol_response->data.value = doubleLittleToBigEndian(sqrt(value));
}

void prepare_date_response(struct prtcl* protocol_request, struct prtcl* protocol_response) {
	time_t actual_time;
	char* actual_time_text;

	protocol_response->type = TYPE_RESPONSE;
	protocol_response->op = protocol_request->op;
	protocol_response->id = protocol_request->id;

	time(&actual_time);
	actual_time_text = ctime(&actual_time);
	strncpy(protocol_response->data.date.value, actual_time_text, strlen(actual_time_text)-1);
	protocol_response->data.date.length = uShortLittleToBigEndian(strlen(actual_time_text)-1);	
}

void* client_connection(void* ptr) {
	struct prtcl protocol_request, protocol_response;
	int client_sockfd = *((int*) ptr);
	int process_id;

	read(client_sockfd, &process_id, sizeof(process_id));
	while(1) {
		read(client_sockfd, &protocol_request, sizeof(struct prtcl));
		if(protocol_request.op == OP_SQRT) {
			prepare_sqrt_response(&protocol_request, &protocol_response);
		}
		else if(protocol_request.op == OP_DATE) {
			prepare_date_response(&protocol_request, &protocol_response);
		}
		write(client_sockfd, &protocol_response, sizeof(struct prtcl));	
		kill(process_id, SIGINT);
	}

	free(ptr);
	return NULL;
}

int main(int argc, char* argv[]) {

	int server_sockfd, client_sockfd, *p_client_sockfd;
	socklen_t server_len, client_len; 					/* unsigned int 32bits */
	struct sockaddr_in server_address, client_address;
	int result;
	pthread_t c_thread;

	printf("Server runs succesfully.\n");

	/* Create a socket for a server */
	server_sockfd = socket(AF_INET, SOCK_STREAM, 0);

	/* Name the socket, as agreed with the clients */
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(6666); 				/* htons() - host to network short */
	server_address.sin_addr.s_addr = htonl(INADDR_ANY); /* htonl() - host to network long */
														/* INADDR_ANY - receive packets destined to any of the interfaces */
	server_len = sizeof(server_address);

	/* Bind */
	result = bind(server_sockfd, (struct sockaddr*) &server_address, server_len);
	if(result == RESULT_ERROR) {
		printf("Bind a server error.\n");
		exit(EXIT_FAILURE);
	}

	/* Communication */
	listen(server_sockfd, 5);
	client_len = sizeof(client_address);
	while(1) {
		puts("The server is waiting...");
		client_sockfd = accept(server_sockfd, (struct sockaddr*) &client_address, &client_len);
		if(client_sockfd != RESULT_ERROR) {
			puts("Connection accepted.");
			p_client_sockfd = malloc(sizeof(int));
			*p_client_sockfd = client_sockfd;
			if(pthread_create(&c_thread, NULL, client_connection, (void*) p_client_sockfd)) {
				puts("Create a thread error.");
				exit(EXIT_FAILURE);
			}
		}
	}
	pthread_join(c_thread, NULL);

	close(server_sockfd);
	exit(EXIT_SUCCESS);
}
